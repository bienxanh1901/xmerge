#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDesktopWidget>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QIntValidator>
#include <QTime>

#define TERM_IN "GPE: incoming"
#define TERM_OUT "GPE: Flush"
#define TERM_MSG_IN "TPT CP Notify"
#define TERM_MSG_OUT "TPT rprts msg to CPB queue"
#define TERM_MSG_OUT_DEST "CIDSERV"
#define TERM_MSG_IN_2 "REPORT sent to CM"
#define OFHK "OFHK"
#define ONHK "ONHK"
#define FLAS "FLAS"
#define ANSWR_MSG "ANSWR_MSG"

#define TAPI_OUT "Sending msg -->"
#define TAPI_IN "Received msg <--"
#define TAPI_INFO "!/1"

#define MSG_IN "INCOMING"
#define MSG_OUT "OUTGOING"
#define ORIGINATION_MSG "ORIGINATION_MSG"
#define DIGITS_MSG "DIGITS_MSG"
#define EXIT_MSG "EXIT_MSG"
#define FLASH_MSG "FLASH_MSG"
#define ANSWER_MSG "ANSWER_MSG"


#define TYPE_IN 1
#define TYPE_OUT 2


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    hasTapiTrace(true),
    hasMSGTrace(false),
    termTraceFile(""),
    tapiTraceFile(""),
    msgTraceFile(""),
    mergeTraceFile(""),
    nodeNumber(-1),
    termNumber(-1),
    timeDelta(0),
    currentDir(QDir::homePath())
{

    ui->setupUi(this);
    QDesktopWidget dw;
    this->setFixedSize(QSize(static_cast<int>(dw.width() * 0.4f), static_cast<int>(dw.height() * 0.4f)));
    this->setWindowTitle(QString("XMerge Tool"));
    this->reset();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_openTermTraceButton_clicked()
{
    QString fileName =  QFileDialog::getOpenFileName(this,
                                                     "Open TermTrace File",
                                                     this->currentDir,"TermTrace Log File (*.log *.txt)");
    if (!fileName.isEmpty()) {
        this->termTraceFile = fileName;
        this->ui->termTraceFileLineEdit->setText(fileName);
        this->currentDir = QFileInfo(fileName).absolutePath();
    }
}

void MainWindow::on_openTapiTraceButton_clicked()
{
    QString fileName =  QFileDialog::getOpenFileName(this,
                                                     "Open TapiTrace File",
                                                     this->currentDir,"TapiTrace Log File (*.log *.txt)");
    if (!fileName.isEmpty()) {
        this->tapiTraceFile = fileName;
        this->ui->tapiTraceFileLineEdit->setText(fileName);
        this->currentDir = QFileInfo(fileName).absolutePath();
    }
}

void MainWindow::on_mergeButton_clicked()
{

    // Check if term file is empty
    if (this->termTraceFile.isEmpty()) {

        QMessageBox::warning(this, "TermTrace File is Required", "Please set the termtrace file");
        return;
    }

    if (this->hasTapiTrace) {
        // Check if tapi file is empty
        if (this->tapiTraceFile.isEmpty()) {
            QMessageBox::warning(this, "TapiTrace File is Required", "Please set the tapitrace file");
            return;
        }
    }

    if (this->hasMSGTrace) {
        // Check if msg file is empty
        if (this->msgTraceFile.isEmpty()) {
            QMessageBox::warning(this, "MsgTrace File is Required", "Please set the MsgTrace file");
            return;
        }

        if (this->nodeNumber == -1) {
            QMessageBox::warning(this, "Node Number is Required", "Please set the Node Number");
            return;
        }

        if (this->termNumber == -1) {
            QMessageBox::warning(this, "Terminal Number is Required", "Please set the Terminal Number");
            return;
        }
    }

    // Get save file name
    QString fileName =  QFileDialog::getSaveFileName(this,
                                                     "Save Merge Trace",
                                                     this->currentDir,"Merge Trace Log File (*.txt)");

    if (!fileName.isEmpty()) {

        this->mergeTraceFile = fileName;
        this->currentDir = QFileInfo(fileName).absolutePath();

        if (this->readTraceFile()) {
            QMessageBox::about(this, "Sussesfull", "Merge Sussessfull!");
        }
    }

}

bool MainWindow::readTraceFile()
{
    QFile term, tapi, msg, merge;
    QTextStream *termIn = nullptr,
                *tapiIn = nullptr,
                *msgIn = nullptr,
                *mergeOut = nullptr;
    term.setFileName(this->termTraceFile);
    if (!term.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::warning(this, "Warning", "Cannot Open TermTrace file: " + term.errorString());
        return false;
    }

    termIn = new QTextStream(&term);

    if (this->hasTapiTrace) {
        tapi.setFileName(this->tapiTraceFile);
        if (!tapi.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QMessageBox::warning(this, "Warning", "Cannot Open TapiTrace file: " + tapi.errorString());
            return false;
        }
        tapiIn = new QTextStream(&tapi);
    }

    if (this->hasMSGTrace) {
        msg.setFileName(this->msgTraceFile);
        if (!msg.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QMessageBox::warning(this, "Warning", "Cannot Open MsgTrace file: " + msg.errorString());
            return false;
        }
        msgIn = new QTextStream(&msg);
    }


    merge.setFileName(this->mergeTraceFile);
    if (!merge.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QMessageBox::warning(this, "Warning", "Cannot Open merger file: " + merge.errorString());
        return false;
    }

    mergeOut = new QTextStream(&merge);
    QString termLine, tapiLine;

    if (hasMSGTrace && !getTimeDelta(this->timeDelta, termIn)) return false;

    while (!termIn->atEnd()) {

        termLine = termIn->readLine();

        if (termLine.contains(TERM_IN)) {

             // check if incomming type is 200
            int messageType = termLine.mid(termLine.indexOf("type: ", 0) + 6, 3).toInt();
            if (messageType == 200 ) {
                *mergeOut << termLine << endl;
                *mergeOut << "" << endl;
                continue;
            }

            if (this->hasTapiTrace) {
                if (!readTapi(TYPE_IN, termLine, tapiIn, mergeOut)) {

                    // show warning
                    return true;

                }
            }
            *mergeOut << termLine << endl;

        } else if (termLine.contains(TERM_OUT)) {

            // print out trace line to file
            *mergeOut << termLine << endl;
            if (this->hasTapiTrace) {
                if (!readTapi(TYPE_OUT, termLine, tapiIn, mergeOut)) {

                    // show warning
                    return true;
                }

            }

        } else if (termLine.contains(TERM_MSG_IN) || (termLine.contains(TERM_MSG_IN_2) && termLine.contains(ANSWR_MSG)) ) {

            *mergeOut << termLine << endl;
            if (this->hasMSGTrace) {
                this->readMsgTrace(TYPE_IN, termLine, msgIn, mergeOut);
            }

        } else if (termLine.contains(TERM_MSG_OUT) && termLine.contains(TERM_MSG_OUT_DEST)) {

            *mergeOut << termLine << endl;
            if (this->hasMSGTrace) {
                this->readMsgTrace(TYPE_OUT, termLine, msgIn, mergeOut);
            }

        } else {
            continue;
        }

        *mergeOut << "" << endl;

    }


    delete termIn;
    term.close();

    if (hasTapiTrace) {
        delete tapiIn;
        tapi.close();
    }

    if (hasMSGTrace) {
        delete msgIn;
        msg.close();
    }

    delete mergeOut;
    merge.close();

    return true;


}

bool MainWindow::readTapi(int type, QString &termLine, QTextStream *tapiIn, QTextStream *mergeOut)
{
    QString line;
    bool ok = false;
    while (!tapiIn->atEnd()) {

        line = tapiIn->readLine();

        // eject wrong line
        if (type == TYPE_IN && !line.contains(TERM_IN)) continue;
        if (type == TYPE_OUT && !line.contains(TERM_OUT)) continue;
        // eject wrong time
        if (!line.startsWith(termLine.left(15))) continue;
        // eject wrong transID
        if (termLine.right(4).toInt(&ok,16) != (line.split(" ").back().toInt())%65536) continue;

        // print out trace line to file
        *mergeOut << line << endl;
        return readTapiInfo(type, tapiIn, mergeOut);
    }

    return false;
}

bool MainWindow::readMsgTrace(int type, QString& termLine, QTextStream *msgIn, QTextStream *mergeOut)
{
    QString line;
    QStringList termList = termLine.split(" ");
    QTime xpmTime = QTime::fromString(termList.at(0).mid(4,8), "hh:mm:ss");
    QString termSignal;
    if (termLine.contains(ANSWR_MSG)) termSignal = ANSWR_MSG;
    else termSignal = termList.at(termList.length() - 2);

    bool ok = false;

    while (!msgIn->atEnd()) {

        line = msgIn->readLine();

        // eject wrong line
        if (type == TYPE_IN && !line.startsWith(MSG_IN)) continue;
        if (type == TYPE_OUT && !line.startsWith(MSG_OUT)) continue;
        QStringList list = line.split(" ");

        // eject wrong time

        QTime ccTime =  QTime::fromString(list.at(2).left(8), "hh:mm:ss");
        int secDelta = xpmTime.secsTo(ccTime);
        if (abs(secDelta - this->timeDelta) > 2) continue;

        // eject wrong signal
        if (type == TYPE_IN) {
            QString msgSignal = list.at(list.length() - 2);
            if (termSignal == OFHK && msgSignal != ORIGINATION_MSG) continue;
            if (termSignal == ONHK && msgSignal != EXIT_MSG) continue;
            if (termSignal == FLAS && msgSignal != FLASH_MSG) continue;
            if (termSignal.length() == 1 && msgSignal != DIGITS_MSG) continue;
            if (termSignal == ANSWR_MSG && msgSignal != ANSWER_MSG) continue;
        }

        QStringList msgTraceInfo = this->readMsgTraceInfo(type, msgIn);
        if (msgTraceInfo.isEmpty()) return false;

        // eject wrong node and term number
        if (type == TYPE_IN)
            list = msgTraceInfo.at(0).split(" ");
        else
            list = msgTraceInfo.at(1).split(" ");

        int nn = list.at(1).toInt(&ok, 16);
        int tn = list.at(4).toInt(&ok, 16);

        if (nn != this->nodeNumber || tn != this->termNumber) continue;

        // eject DE E2 message (temp)
        if (type == TYPE_OUT) {
            if (msgTraceInfo.at(3).startsWith("DE E2")) continue;
        }
        // print out trace line to file
        *mergeOut << line << endl;
        for (int i = 0; i < msgTraceInfo.length(); i++) {
            *mergeOut << msgTraceInfo.at(i) << endl;
        }

        return true;
    }

    return false;


}

bool MainWindow::readTapiInfo(int type, QTextStream* tapiIn, QTextStream* mergeOut)
{
    QString line;
    while (!tapiIn->atEnd()) {
        line  = tapiIn->readLine();
        if ((type == TYPE_IN && line.contains(TAPI_IN)) ||
            (type == TYPE_OUT && line.contains(TAPI_OUT))) {
            *mergeOut << line << endl;

            while (!tapiIn->atEnd()) {
                line = tapiIn->readLine();
                *mergeOut << line << endl;
                if (line.endsWith("}")) break;
            }

            return true;
        }

    }

    return false;

}

QStringList MainWindow::readMsgTraceInfo(int type, QTextStream *msgIn)
{
    QString line;
    QStringList list;
    int cnt = 0;
    while (!msgIn->atEnd()) {
        line  = msgIn->readLine();
        if (line == " ") {
            cnt++;
            if (cnt == type) return list;
        }
        list.append(line);
    }

    return list;

}

bool MainWindow::getTimeDelta(int& secDelta, QTextStream *termIn)
{
    QString line;
    while (!termIn->atEnd()) {

        line = termIn->readLine();
        if (line.contains("XPM time_w") && line.contains("CC time_w")) {
            QStringList list = line.split(" ");
            QTime xpmTime = QTime::fromString(list.at(3).mid(4,8), "hh:mm:ss");
            QTime ccTime = QTime::fromString(list.at(9).mid(4,8), "hh:mm:ss");
            secDelta = xpmTime.secsTo(ccTime);
            return true;
        }
    }

    return false;
}

void MainWindow::reset()
{
    this->tapiTraceFile = "";
    this->termTraceFile = "";
    this->msgTraceFile = "";
    this->mergeTraceFile = "";
    this->nodeNumber = -1;
    this->termNumber = -1;
    this->ui->tapiTraceFileLineEdit->setText("");
    this->ui->termTraceFileLineEdit->setText("");
    this->ui->msgTraceFileLineEdit->setText("");
    this->ui->nodeNumberLineEdit->setValidator( new QIntValidator(0, 100000, this) );
    this->ui->termNumberLineEdit->setValidator( new QIntValidator(0, 100000, this) );
    this->ui->msgTraceCheckBox->setCheckState(Qt::Unchecked);
    this->deactiveMsgTrace();
    this->ui->tapiTraceCheckBox->setCheckState(Qt::Checked);

}

void MainWindow::activeMsgTrace()
{
    this->hasMSGTrace = true;
    this->ui->nodeNumberLineEdit->setEnabled(true);
    this->ui->termNumberLineEdit->setEnabled(true);
    this->ui->msgTraceFileLineEdit->setEnabled(true);
    this->ui->openMsgTraceButton->setEnabled(true);
}

void MainWindow::deactiveMsgTrace()
{
    this->hasMSGTrace = false;
    this->ui->nodeNumberLineEdit->setEnabled(false);
    this->ui->termNumberLineEdit->setEnabled(false);
    this->ui->msgTraceFileLineEdit->setEnabled(false);
    this->ui->openMsgTraceButton->setEnabled(false);
}

void MainWindow::activeTapiTrace()
{
    this->hasTapiTrace = true;
    this->ui->tapiTraceFileLineEdit->setEnabled(true);
    this->ui->openTapiTraceButton->setEnabled(true);
}

void MainWindow::deactiveTapiTrace()
{
    this->hasTapiTrace = false;
    this->ui->tapiTraceFileLineEdit->setEnabled(false);
    this->ui->openTapiTraceButton->setEnabled(false);
}


void MainWindow::on_openMsgTraceButton_clicked()
{
    QString fileName =  QFileDialog::getOpenFileName(this,
                                                     "Open MsgTrace File",
                                                    this->currentDir,"MsgTrace Log File (*.log *.txt)");
    if (!fileName.isEmpty()) {
        this->msgTraceFile = fileName;
        this->ui->msgTraceFileLineEdit->setText(fileName);
        this->currentDir = QFileInfo(fileName).absolutePath();
    }

}

void MainWindow::on_tapiTraceCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked) {

        this->deactiveTapiTrace();
        if (!this->hasMSGTrace) {
            this->ui->msgTraceCheckBox->setCheckState(Qt::Checked);
        }
    } else if (arg1 == Qt::Checked) {
        this->activeTapiTrace();
    }
}

void MainWindow::on_msgTraceCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked) {

        this->deactiveMsgTrace();
        if (!this->hasMSGTrace) {

            this->ui->tapiTraceCheckBox->setCheckState(Qt::Checked);
        }
    } else if (arg1 == Qt::Checked) {
        this->activeMsgTrace();
    }
}

void MainWindow::on_termTraceFileLineEdit_editingFinished()
{
    this->termTraceFile = this->ui->termNumberLineEdit->text();
}

void MainWindow::on_tapiTraceFileLineEdit_editingFinished()
{
    this->tapiTraceFile = this->ui->tapiTraceFileLineEdit->text();
}

void MainWindow::on_nodeNumberLineEdit_editingFinished()
{
    this->nodeNumber = this->ui->nodeNumberLineEdit->text().toInt();
}

void MainWindow::on_termNumberLineEdit_editingFinished()
{
    this->termNumber = this->ui->termNumberLineEdit->text().toInt();
}

void MainWindow::on_msgTraceFileLineEdit_editingFinished()
{
    this->msgTraceFile = this->ui->msgTraceFileLineEdit->text();
}
