#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QTextStream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void on_openTermTraceButton_clicked();

    void on_openTapiTraceButton_clicked();

    void on_mergeButton_clicked();

    void on_openMsgTraceButton_clicked();

    void on_tapiTraceCheckBox_stateChanged(int arg1);

    void on_msgTraceCheckBox_stateChanged(int arg1);

    void on_termTraceFileLineEdit_editingFinished();

    void on_tapiTraceFileLineEdit_editingFinished();

    void on_nodeNumberLineEdit_editingFinished();

    void on_termNumberLineEdit_editingFinished();

    void on_msgTraceFileLineEdit_editingFinished();

private:
    Ui::MainWindow *ui;
    bool hasTapiTrace;
    bool hasMSGTrace;
    QString termTraceFile;
    QString tapiTraceFile;
    QString msgTraceFile;
    QString mergeTraceFile;
    int nodeNumber;
    int termNumber;
    int timeDelta;
    QString currentDir;

    bool readTraceFile();
    bool readTapi(int type, QString &termLine, QTextStream* tapiIn, QTextStream* mergeOut);
    bool readMsgTrace(int type, QString& termLine, QTextStream* msgIn, QTextStream* mergeOut);
    bool readTapiInfo(int type, QTextStream* tapiIn, QTextStream* mergeOut);
    QStringList readMsgTraceInfo(int type, QTextStream* msgIn);
    bool getTimeDelta(int& secDelta, QTextStream* termIn);
    void reset();
    void activeMsgTrace();
    void deactiveMsgTrace();
    void activeTapiTrace();
    void deactiveTapiTrace();
};

#endif // MAINWINDOW_H
